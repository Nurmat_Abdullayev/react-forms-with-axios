import React from "react";
import { ReactComponent as ActionIcon } from "../../Assets/Images/action.svg";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";
import "./JobType.css";

const JobType = ({ jobTypes, deleteJobType, isDisabled }) => {
	const submitButton = isDisabled ? <MButton BType="disabled">Сохранить</MButton> : <MButton type="submit">Сохранить</MButton>
	return (
		<div className="jobType block">
			<h1>Job type</h1>
			<hr />
			<div className="static default">
				<div className="item">№</div>
				<div className="item">Label</div>
				<div className="item">
					<ActionIcon className="item action action__btn" />
				</div>
			</div>
			{jobTypes &&
				jobTypes?.map((jobType) => (
					<div className="default" key={jobType?.id}>
						<div className="item">{jobType?.id}</div>
						<div className="item">{jobType?.label} </div>
						{isDisabled ?
							<MButton
								onClick={() => {
									deleteJobType(jobType?.id)
								}}
								BClass="item action action__delete"
								BType={"disabled"}
								BIcon={<DeleteIcon />}
							/>
							: <MButton
								onClick={() => {
									deleteJobType(jobType?.id)
								}}
								BClass="item action action__delete"
								BType={"outline-primary"}
								BIcon={<DeleteIcon />}
							/>}
					</div>
				))}
		</div>
	);
};

export default JobType;
